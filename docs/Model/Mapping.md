# # Mapping

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**start_row** | **float** | Première ligne de données | [optional]
**data** | [**\OpenAPI\Client\Model\MappingDataInner[]**](MappingDataInner.md) | Mapping des colonnes | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
