# # MappingDataInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**column** | **float[]** |  | [optional]
**field** | **string** | champ du destinataire | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
