# # RecipientUpdate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**custom_id** | **string** | Identifiant du destinataire fourni par le client | [optional]
**reference_to_print** | **string** | référence client imprimable | [optional]
**custom_data** | **string** | Information libre fournie par le client | [optional]
**address_line_1** | **string** | Ligne d&#39;adresse n°1 (Société) | [optional]
**address_line_2** | **string** | Ligne d&#39;adresse n°2 (Civilité, Prénom, Nom) | [optional]
**address_line_3** | **string** | Ligne d&#39;adresse n°3 (Résidence, Bâtiment ...) | [optional]
**address_line_4** | **string** | Ligne d&#39;adresse n°4 (N° et libellé de la voie) | [optional]
**address_line_5** | **string** | Ligne d&#39;adresse n°5 (Lieu dit, BP...) | [optional]
**address_line_6** | **string** | Ligne d&#39;adresse n°6 (Code postal et ville) | [optional]
**custom_fields** | **array<string,string>** | Champs de personnalisation du destinataire | [optional]
**country_code** | **string** | Code du pays (Code ISO-3166) codes autorisés  FR, MC, RE, YT, MQ, GP, MF, BL, GF | [optional]
**reference** | **string** | référence Maileva du pli | [optional]
**documents_override** | [**\OpenAPI\Client\Model\DocumentsOverrideItem[]**](DocumentsOverrideItem.md) | Liste de bribes de documents. Si ce champ n&#39;est pas renseigné, le destinataire recevra tous les documents associés à l&#39;envoi. Si ce champ est renseigné, le destinataire recevra la liste des bribes de documents indiquées (dans l&#39;ordre des éléments du tableau). | [optional]
**scheduled_date** | **string** | Date de planification | [optional]
**envelope_type** | [**\OpenAPI\Client\Model\EnvelopeType**](EnvelopeType.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
