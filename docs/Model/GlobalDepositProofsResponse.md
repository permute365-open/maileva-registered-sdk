# # GlobalDepositProofsResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**global_deposit_proofs** | [**\OpenAPI\Client\Model\GlobalDepositProofResponse[]**](GlobalDepositProofResponse.md) |  |
**paging** | [**\OpenAPI\Client\Model\PagingResponse**](PagingResponse.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
