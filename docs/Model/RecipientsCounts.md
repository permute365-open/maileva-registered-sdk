# # RecipientsCounts

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**draft** | **int** | Nombre de destinataires au statut DRAFT | [optional]
**pending** | **int** | Nombre de destinataires au statut PENDING | [optional]
**blocked** | **int** | Nombre de destinataires au statut BLOCKED | [optional]
**accepted** | **int** | Nombre de destinataires au statut ACCEPTED | [optional]
**processed** | **int** | Nombre de destinataires au statut PROCESSED | [optional]
**sending_rejected** | **int** | Nombre de destinataires au statut SENDING_REJECTED | [optional]
**canceled** | **int** | Nombre de destinataires au statut CANCELED | [optional]
**preparing** | **int** | Nombre de destinataires au statut PREPARING | [optional]
**rejected** | **int** | Nombre de destinataires au statut REJECTED | [optional]
**total** | **int** | Nombre de destinataires total | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
