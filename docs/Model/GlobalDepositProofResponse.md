# # GlobalDepositProofResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Identifiant de la preuve de dépôt | [optional]
**date** | **\DateTime** | Date de la preuve de dépôt | [optional]
**sending_id** | **string** | Identifiant de l&#39;envoi associé à la preuve de dépôt | [optional]
**url** | **string** | Lien de téléchargement de la preuve de dépôt | [optional]
**archive_date** | **\DateTime** | Date d&#39;archivage de la preuve de dépôt | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
