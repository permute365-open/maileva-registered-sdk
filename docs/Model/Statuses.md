# # Statuses

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date** | **\DateTime** | Date du statut | [optional]
**code** | **string** | Code du statut | [optional]
**details** | **string** | Statut détaillé | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
