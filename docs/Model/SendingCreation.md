# # SendingCreation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Nom de l&#39;envoi |
**custom_id** | **string** | Identifiant de l&#39;envoi défini par le client | [optional]
**reference_to_print_enabled** | **bool** | Possibilité d&#39;imprimer une référence client | [optional] [default to false]
**custom_data** | **string** | Information libre fournie par le client | [optional]
**acknowledgement_of_receipt** | **bool** | Avis de réception (AR) | [optional] [default to true]
**expected_date** | **string** | Date de planification choisie par le client (à 30j max) | [optional]
**color_printing** | **bool** | Impression couleur | [optional] [default to true]
**duplex_printing** | **bool** | Impression recto verso | [optional] [default to true]
**optional_address_sheet** | **bool** | Feuille porte-adresse optionnelle | [optional] [default to false]
**notification_email** | **string** | E-mail du destinataire des notifications |
**print_sender_address** | **bool** | Impression de l&#39;adresse expéditeur | [optional] [default to false]
**returned_mail_scanning** | **bool** | Gestion numérique des PND (plis non distribuables) | [optional] [default to false]
**sender_address_line_1** | **string** | Ligne d&#39;adresse n°1 (Société) de l&#39;expéditeur | [optional]
**sender_address_line_2** | **string** | Ligne d&#39;adresse n°2 (Civilité, Prénom, Nom) de l&#39;expéditeur | [optional]
**sender_address_line_3** | **string** | Ligne d&#39;adresse n°3 (Résidence, Bâtiment ...) de l&#39;expéditeur | [optional]
**sender_address_line_4** | **string** | Ligne d&#39;adresse n°4 (N° et libellé de la voie) de l&#39;expéditeur | [optional]
**sender_address_line_5** | **string** | Ligne d&#39;adresse n°5 (Lieu dit, BP...) de l&#39;expéditeur | [optional]
**sender_address_line_6** | **string** | Ligne d&#39;adresse n°6 (Code postal et ville) de l&#39;expéditeur | [optional]
**sender_country_code** | **string** | Code du pays (Code ISO-3166). Codes autorisés FR, MC, RE, YT, MQ, GP, MF, BL, GF | [optional]
**archiving_duration** | **int** | Durée d&#39;archivage en années, durées autorisées 3, 6, 10 | [optional] [default to 3]
**postage_type** | **string** | Type d&#39;affranchissement. Valeurs autorisées FAST, URGENT. FAST distribution en 3 jours et URGENT distribution en 2 jours. | [optional] [default to 'FAST']

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
