# # MainDeliveryStatusResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date** | **\DateTime** | Date de l&#39;évènement | [optional]
**category** | **string** | Catégorie de l&#39;évènement. Valeurs possibles DELIVERING, FIRST_PRESENTATION, DELIVERED, UNDELIVERED. &lt;table border&#x3D;\&quot;1\&quot;&gt; &lt;tr bgcolor&#x3D;\&quot;lightgrey\&quot;&gt; &lt;th&gt;Category&lt;/th&gt; &lt;th&gt;Signification&lt;/th&gt; &lt;/tr&gt; &lt;tr&gt;&lt;td&gt;DELIVERING&lt;/td&gt;&lt;td&gt;En distribution&lt;/td&gt;&lt;/tr&gt; &lt;tr&gt;&lt;td&gt;FIRST_PRESENTATION&lt;/td&gt;&lt;td&gt;Première présentation&lt;/td&gt;&lt;/tr&gt; &lt;tr&gt;&lt;td&gt;DELIVERED&lt;/td&gt;&lt;td&gt;Distribution&lt;/td&gt;&lt;/tr&gt; &lt;tr&gt;&lt;td&gt;UNDELIVERED&lt;/td&gt;&lt;td&gt;Non distribuable&lt;/td&gt;&lt;/tr&gt; &lt;/table&gt; | [optional]
**location** | **string** | Lieu de l&#39;évènement | [optional]
**code** | **string** | Code MAILEVA du statut de distribution | [optional]
**label** | **string** | Libellé du statut de distribution | [optional]
**source** | **string** | Fournisseur de l&#39;évènement | [optional]
**code_provider** | **string** | Code fournisseur du statut de distribution. Cette codification est donnée à titre informatif et est susceptible de changer. | [optional]
**creation_date** | **\DateTime** | Date de création de la ressource | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
