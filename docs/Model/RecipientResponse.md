# # RecipientResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Identifiant du destinataire |
**custom_id** | **string** | Identifiant du destinataire fourni par le client | [optional]
**reference_to_print** | **string** | référence client imprimable | [optional]
**custom_data** | **string** | Information libre fournie par le client | [optional]
**address_line_1** | **string** | Ligne d&#39;adresse n°1 (Société) | [optional]
**address_line_2** | **string** | Ligne d&#39;adresse n°2 (Civilité, Prénom, Nom) | [optional]
**address_line_3** | **string** | Ligne d&#39;adresse n°3 (Résidence, Bâtiment ...) | [optional]
**address_line_4** | **string** | Ligne d&#39;adresse n°4 (N° et libellé de la voie) | [optional]
**address_line_5** | **string** | Ligne d&#39;adresse n°5 (Lieu dit, BP...) | [optional]
**address_line_6** | **string** | Ligne d&#39;adresse n°6 (Code postal et ville) |
**country_code** | **string** | Code du pays (Code ISO-3166). Codes autorisés FR, MC, RE, YT, MQ, GP, MF, BL, GF |
**reference** | **string** | référence Maileva du pli. | [optional]
**envelope_type** | [**\OpenAPI\Client\Model\EnvelopeType**](EnvelopeType.md) |  | [optional]
**documents_override** | [**\OpenAPI\Client\Model\DocumentsOverrideItem[]**](DocumentsOverrideItem.md) | Liste de bribes de documents. Si ce champ n&#39;est pas renseigné, le destinataire recevra tous les documents associés à l&#39;envoi. Si ce champ est renseigné, le destinataire recevra la liste des bribes de documents indiquées (dans l&#39;ordre des éléments du tableau). | [optional]
**status** | [**\OpenAPI\Client\Model\RecipientStatus**](RecipientStatus.md) |  |
**status_detail** | **string** | Détail d&#39;un statut (cause du rejet) | [optional]
**statuses** | [**\OpenAPI\Client\Model\Statuses[]**](Statuses.md) | Liste des statuts | [optional]
**scheduled_date** | **string** | Date de planification | [optional]
**deposit_proof_scheduled_date** | **string** | Date de planification | [optional]
**scheduled_delivery_date** | **string** | Date de planification de remise | [optional]
**last_delivery_status** | **string** | Dernier statut de distribution | [optional]
**last_delivery_status_date** | **\DateTime** | Date du dernier statut de distribution | [optional]
**postage_price** | **float** | Coût de l&#39;affranchissement en euros | [optional]
**postage_class** | [**\OpenAPI\Client\Model\PostageClass**](PostageClass.md) |  | [optional]
**registered_number** | **string** | Numéro de recommandé | [optional]
**content_proof_embedded_document_url** | **string** | URL du document contenu dans la preuve de contenu | [optional]
**deposit_proof_date** | **\DateTime** | Date de la preuve de dépôt | [optional]
**deposit_proof_archive_date** | **\DateTime** | Date d&#39;archivage de la preuve de dépôt | [optional]
**deposit_proof_url** | **string** | URL de l&#39;archive de la preuve de dépôt. Attendre que cet attribut soit renseigné pour lancer le téléchargement. | [optional]
**global_deposit_proof_id** | **string** | Identifiant de la preuve de dépôt globale | [optional]
**content_proof_date** | **\DateTime** | Date de la preuve de contenu. Attendre que cet attribut soit renseigné pour lancer le téléchargement. | [optional]
**content_proof_archive_date** | **\DateTime** | Date d&#39;archivage de la preuve de contenu | [optional]
**content_proof_url** | **string** | URL de l&#39;archive de la preuve de contenu | [optional]
**acknowledgement_of_receipt_date** | **\DateTime** | Date de l&#39;avis de réception (AR). Attendre que cet attribut soit renseigné pour lancer le téléchargement. | [optional]
**acknowledgement_of_receipt_archive_date** | **\DateTime** | Date d&#39;archivage de l&#39;avis de réception (AR) | [optional]
**acknowledgement_of_receipt_url** | **string** | URL de l&#39;archive de l&#39;avis de réception | [optional]
**undelivered_mail_date** | **\DateTime** | Date du pli non distribuable (PND). Attendre que cet attribut soit renseigné pour lancer le téléchargement. | [optional]
**undelivered_mail_archive_date** | **\DateTime** | Date d&#39;archivage du pli non distribuable (PND) | [optional]
**undelivered_mail_url** | **string** | URL de l&#39;archive du pli non distribuable (PND) | [optional]
**undelivered_mail_reason** | **string** | Raison du pli non distribuable (PND) &lt;table&gt;   &lt;tr&gt;     &lt;td&gt;N02&lt;/td&gt;     &lt;td&gt;Destinataire inconnu à l&#39;adresse&lt;/td&gt;   &lt;/tr&gt;   &lt;tr&gt;     &lt;td&gt;N03&lt;/td&gt;     &lt;td&gt;Pli avisé et non réclamé&lt;/td&gt;   &lt;/tr&gt;   &lt;tr&gt;     &lt;td&gt;N05&lt;/td&gt;     &lt;td&gt;Pli refusé par le destinataire&lt;/td&gt;   &lt;/tr&gt;   &lt;tr&gt;     &lt;td&gt;N07&lt;/td&gt;     &lt;td&gt;Défaut d&#39;accès ou d&#39;adressage&lt;/td&gt;   &lt;/tr&gt; &lt;/table&gt; | [optional]
**pages_count** | **int** | Nombre de pages. Ce nombre de pages inclut l&#39;éventuelle page porte-adresse (payante ou obligatoire) mais n&#39;inclut pas les pages blanches ajoutées au verso par Maileva. | [optional]
**billed_pages_count** | **int** | Nombre de pages facturées (disponible à partir du statut ACCEPTED). Ce nombre de pages inclut la page porte-adresse payante (DL) mais n&#39;inclut pas la page porte-adresse obligatoire (C4) ni les pages blanches ajoutées au verso par Maileva. | [optional]
**sheets_count** | **int** | Nombre de feuilles (disponible à partir du statut ACCEPTED). Ce nombre de feuilles inclut la page porte-adresse éventuelle (payante ou obligatoire). | [optional]
**delivery_statuses** | [**\OpenAPI\Client\Model\DeliveryStatus[]**](DeliveryStatus.md) | Liste des delivery statuses | [optional]
**main_delivery_statuses** | [**\OpenAPI\Client\Model\MainDeliveryStatusResponse[]**](MainDeliveryStatusResponse.md) | Liste des main delivery statuses | [optional]
**custom_fields** | **array<string,string>** | Champs de personnalisation du destinataire | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
