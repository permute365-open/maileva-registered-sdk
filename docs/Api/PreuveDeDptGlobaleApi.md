# OpenAPI\Client\PreuveDeDptGlobaleApi

All URIs are relative to https://api.sandbox.maileva.net/registered_mail/v4, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**downloadGlobalDepositProof()**](PreuveDeDptGlobaleApi.md#downloadGlobalDepositProof) | **GET** /global_deposit_proofs/{global_deposit_proof_id}/download | Télécharger une preuve de dépôt globale |
| [**getAllGlobalDepositProofs()**](PreuveDeDptGlobaleApi.md#getAllGlobalDepositProofs) | **GET** /global_deposit_proofs | Liste des preuves de dépôt globales |
| [**getGlobalDepositProof()**](PreuveDeDptGlobaleApi.md#getGlobalDepositProof) | **GET** /global_deposit_proofs/{global_deposit_proof_id} | Détail d&#39;une preuve de dépôt globale |


## `downloadGlobalDepositProof()`

```php
downloadGlobalDepositProof($global_deposit_proof_id): \SplFileObject
```

Télécharger une preuve de dépôt globale

Cette API permet de télécharger au format PDF une preuve de dépôt globale

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: oAuth2PasswordProduction
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oAuth2PasswordSandbox
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\PreuveDeDptGlobaleApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$global_deposit_proof_id = 'global_deposit_proof_id_example'; // string | Identifiant d'une preuve de dépôt globale

try {
    $result = $apiInstance->downloadGlobalDepositProof($global_deposit_proof_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PreuveDeDptGlobaleApi->downloadGlobalDepositProof: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **global_deposit_proof_id** | **string**| Identifiant d&#39;une preuve de dépôt globale | |

### Return type

**\SplFileObject**

### Authorization

[oAuth2PasswordProduction](../../README.md#oAuth2PasswordProduction), [oAuth2PasswordSandbox](../../README.md#oAuth2PasswordSandbox), [bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/pdf`, `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getAllGlobalDepositProofs()`

```php
getAllGlobalDepositProofs($start_index, $count, $sending_id): \OpenAPI\Client\Model\GlobalDepositProofsResponse
```

Liste des preuves de dépôt globales

Cette API permet de lister les preuves de dépôt globales

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: oAuth2PasswordProduction
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oAuth2PasswordSandbox
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\PreuveDeDptGlobaleApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$start_index = 1; // int | Le premier élément à retourner
$count = 50; // int | Le nombre d'élément à retourner
$sending_id = 'sending_id_example'; // string | Filtre sur le sending_id

try {
    $result = $apiInstance->getAllGlobalDepositProofs($start_index, $count, $sending_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PreuveDeDptGlobaleApi->getAllGlobalDepositProofs: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **start_index** | **int**| Le premier élément à retourner | [optional] [default to 1] |
| **count** | **int**| Le nombre d&#39;élément à retourner | [optional] [default to 50] |
| **sending_id** | **string**| Filtre sur le sending_id | [optional] |

### Return type

[**\OpenAPI\Client\Model\GlobalDepositProofsResponse**](../Model/GlobalDepositProofsResponse.md)

### Authorization

[oAuth2PasswordProduction](../../README.md#oAuth2PasswordProduction), [oAuth2PasswordSandbox](../../README.md#oAuth2PasswordSandbox), [bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getGlobalDepositProof()`

```php
getGlobalDepositProof($global_deposit_proof_id): \OpenAPI\Client\Model\GlobalDepositProofResponse
```

Détail d'une preuve de dépôt globale

Permet d'obtenir le détail d'une preuve de preuve de dépôt globale

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: oAuth2PasswordProduction
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oAuth2PasswordSandbox
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\PreuveDeDptGlobaleApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$global_deposit_proof_id = 'global_deposit_proof_id_example'; // string | Identifiant d'une preuve de dépôt globale

try {
    $result = $apiInstance->getGlobalDepositProof($global_deposit_proof_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PreuveDeDptGlobaleApi->getGlobalDepositProof: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **global_deposit_proof_id** | **string**| Identifiant d&#39;une preuve de dépôt globale | |

### Return type

[**\OpenAPI\Client\Model\GlobalDepositProofResponse**](../Model/GlobalDepositProofResponse.md)

### Authorization

[oAuth2PasswordProduction](../../README.md#oAuth2PasswordProduction), [oAuth2PasswordSandbox](../../README.md#oAuth2PasswordSandbox), [bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
