# OpenAPI\Client\EnvoiApi

All URIs are relative to https://api.sandbox.maileva.net/registered_mail/v4, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**deleteSending()**](EnvoiApi.md#deleteSending) | **DELETE** /sendings/{sending_id} | Suppression d&#39;un envoi |
| [**getAllSendings()**](EnvoiApi.md#getAllSendings) | **GET** /sendings | Liste des envois |
| [**getSending()**](EnvoiApi.md#getSending) | **GET** /sendings/{sending_id} | Détail d&#39;un envoi |
| [**postSending()**](EnvoiApi.md#postSending) | **POST** /sendings | Création d&#39;un envoi |
| [**submitSending()**](EnvoiApi.md#submitSending) | **POST** /sendings/{sending_id}/submit | Finalisation d&#39;un envoi |
| [**updateSending()**](EnvoiApi.md#updateSending) | **PATCH** /sendings/{sending_id} | Modification partielle d&#39;un envoi |


## `deleteSending()`

```php
deleteSending($sending_id)
```

Suppression d'un envoi

Permet de supprimer un envoi.  Seuls les envois en état de brouillon (DRAFT) peuvent être supprimés.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: oAuth2PasswordProduction
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oAuth2PasswordSandbox
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\EnvoiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sending_id = 'sending_id_example'; // string | Identifiant d'un envoi

try {
    $apiInstance->deleteSending($sending_id);
} catch (Exception $e) {
    echo 'Exception when calling EnvoiApi->deleteSending: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sending_id** | **string**| Identifiant d&#39;un envoi | |

### Return type

void (empty response body)

### Authorization

[oAuth2PasswordProduction](../../README.md#oAuth2PasswordProduction), [oAuth2PasswordSandbox](../../README.md#oAuth2PasswordSandbox), [bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getAllSendings()`

```php
getAllSendings($start_index, $count): \OpenAPI\Client\Model\SendingsResponse
```

Liste des envois

Permet d'obtenir la liste des envois.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: oAuth2PasswordProduction
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oAuth2PasswordSandbox
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\EnvoiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$start_index = 1; // int | Le premier élément à retourner
$count = 50; // int | Le nombre d'élément à retourner

try {
    $result = $apiInstance->getAllSendings($start_index, $count);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EnvoiApi->getAllSendings: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **start_index** | **int**| Le premier élément à retourner | [optional] [default to 1] |
| **count** | **int**| Le nombre d&#39;élément à retourner | [optional] [default to 50] |

### Return type

[**\OpenAPI\Client\Model\SendingsResponse**](../Model/SendingsResponse.md)

### Authorization

[oAuth2PasswordProduction](../../README.md#oAuth2PasswordProduction), [oAuth2PasswordSandbox](../../README.md#oAuth2PasswordSandbox), [bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getSending()`

```php
getSending($sending_id): \OpenAPI\Client\Model\SendingResponse
```

Détail d'un envoi

Permet de récupérer le détail d'un envoi à partir de son identifiant.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: oAuth2PasswordProduction
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oAuth2PasswordSandbox
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\EnvoiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sending_id = 'sending_id_example'; // string | Identifiant d'un envoi

try {
    $result = $apiInstance->getSending($sending_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EnvoiApi->getSending: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sending_id** | **string**| Identifiant d&#39;un envoi | |

### Return type

[**\OpenAPI\Client\Model\SendingResponse**](../Model/SendingResponse.md)

### Authorization

[oAuth2PasswordProduction](../../README.md#oAuth2PasswordProduction), [oAuth2PasswordSandbox](../../README.md#oAuth2PasswordSandbox), [bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `postSending()`

```php
postSending($sending_creation): \OpenAPI\Client\Model\SendingResponse
```

Création d'un envoi

Permet de créer un envoi. Cet envoi sera en état de brouillon (DRAFT), il faudra soummettre cet envoi pour qu'il soit envoyé en production.  Les principales options sont : - L'avis de réception (AR) : oui ou non, - Le coloris d'impression : couleur ou noir et blanc, - Le format d'impression : recto-verso ou recto seul, - L'ajout d'une page porte-adresse, - Le type d'enveloppe est choisi automatiquement. 1 à 5 feuilles (feuille porte-adresse et enveloppe retour incluses) : enveloppe DL. 6 à 44 feuilles (hors feuille porte-adresse, enveloppe retour incluse) : enveloppe C4. - Numérisation des plis non distribuables (PND) : oui ou non, - Durée d'archivage : 3 ans, 6 ans ou 10 ans, - Notifications par e-mail : adresse e-mail à laquelle la preuve de dépôt et les notifications doivent être envoyées

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: oAuth2PasswordProduction
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oAuth2PasswordSandbox
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\EnvoiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sending_creation = new \OpenAPI\Client\Model\SendingCreation(); // \OpenAPI\Client\Model\SendingCreation

try {
    $result = $apiInstance->postSending($sending_creation);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EnvoiApi->postSending: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sending_creation** | [**\OpenAPI\Client\Model\SendingCreation**](../Model/SendingCreation.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\SendingResponse**](../Model/SendingResponse.md)

### Authorization

[oAuth2PasswordProduction](../../README.md#oAuth2PasswordProduction), [oAuth2PasswordSandbox](../../README.md#oAuth2PasswordSandbox), [bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `submitSending()`

```php
submitSending($sending_id)
```

Finalisation d'un envoi

Permet de soumettre l'envoi et de déclencher ainsi la demande de production. Un envoi soumis ne peut pas être annulé.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: oAuth2PasswordProduction
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oAuth2PasswordSandbox
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\EnvoiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sending_id = 'sending_id_example'; // string | Identifiant d'un envoi

try {
    $apiInstance->submitSending($sending_id);
} catch (Exception $e) {
    echo 'Exception when calling EnvoiApi->submitSending: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sending_id** | **string**| Identifiant d&#39;un envoi | |

### Return type

void (empty response body)

### Authorization

[oAuth2PasswordProduction](../../README.md#oAuth2PasswordProduction), [oAuth2PasswordSandbox](../../README.md#oAuth2PasswordSandbox), [bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `updateSending()`

```php
updateSending($sending_id, $sending_update): \OpenAPI\Client\Model\SendingResponse
```

Modification partielle d'un envoi

Permet de modifier un envoi.  Seuls les envois en état de brouillon (DRAFT) peuvent être modifiés.  Seuls les paramètres pour lesquels une valeur est fournie sont modifiés.  Si votre système ne permet pas d'utiliser le verbe PATCH, il est possible d'utiliser le verbe POST.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: oAuth2PasswordProduction
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oAuth2PasswordSandbox
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\EnvoiApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sending_id = 'sending_id_example'; // string | Identifiant d'un envoi
$sending_update = new \OpenAPI\Client\Model\SendingUpdate(); // \OpenAPI\Client\Model\SendingUpdate

try {
    $result = $apiInstance->updateSending($sending_id, $sending_update);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EnvoiApi->updateSending: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sending_id** | **string**| Identifiant d&#39;un envoi | |
| **sending_update** | [**\OpenAPI\Client\Model\SendingUpdate**](../Model/SendingUpdate.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\SendingResponse**](../Model/SendingResponse.md)

### Authorization

[oAuth2PasswordProduction](../../README.md#oAuth2PasswordProduction), [oAuth2PasswordSandbox](../../README.md#oAuth2PasswordSandbox), [bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: `application/json`, `application/merge-patch+json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
