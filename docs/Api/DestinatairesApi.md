# OpenAPI\Client\DestinatairesApi

All URIs are relative to https://api.sandbox.maileva.net/registered_mail/v4, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**createRecipientOfSending()**](DestinatairesApi.md#createRecipientOfSending) | **POST** /sendings/{sending_id}/recipients | Ajout d&#39;un destinataire à l&#39;envoi |
| [**deleteAllRecipientsOfSending()**](DestinatairesApi.md#deleteAllRecipientsOfSending) | **DELETE** /sendings/{sending_id}/recipients | Suppression de tous les destinataires |
| [**deleteRecipientOfSending()**](DestinatairesApi.md#deleteRecipientOfSending) | **DELETE** /sendings/{sending_id}/recipients/{recipient_id} | Suppression d&#39;un destinataire |
| [**downloadAcknowledgementOfReceiptOfRecipientOfSending()**](DestinatairesApi.md#downloadAcknowledgementOfReceiptOfRecipientOfSending) | **GET** /sendings/{sending_id}/recipients/{recipient_id}/download_acknowledgement_of_receipt | Télécharger l&#39;avis de réception archivé du destinataire |
| [**downloadArchivedUndeliveredMailOfRecipientOfSending()**](DestinatairesApi.md#downloadArchivedUndeliveredMailOfRecipientOfSending) | **GET** /sendings/{sending_id}/recipients/{recipient_id}/download_undelivered_mail | Téléchargement de l&#39;image du pli retourné |
| [**downloadContentProofOfRecipientOfSending()**](DestinatairesApi.md#downloadContentProofOfRecipientOfSending) | **GET** /sendings/{sending_id}/recipients/{recipient_id}/download_content_proof | Télécharger la preuve de contenu du destinataire |
| [**downloadDepositProofOfRecipientOfSending()**](DestinatairesApi.md#downloadDepositProofOfRecipientOfSending) | **GET** /sendings/{sending_id}/recipients/{recipient_id}/download_deposit_proof | Télécharger la preuve de dépôt du destinataire |
| [**downloadEmbeddedDocumentOfRecipientOfSending()**](DestinatairesApi.md#downloadEmbeddedDocumentOfRecipientOfSending) | **GET** /sendings/{sending_id}/recipients/{recipient_id}/content_proof/download_embedded_document | Télécharger le document contenu dans la preuve de contenu |
| [**getAllRecipientsOfSending()**](DestinatairesApi.md#getAllRecipientsOfSending) | **GET** /sendings/{sending_id}/recipients | Liste des destinataires d&#39;un envoi |
| [**getRecipientOfSending()**](DestinatairesApi.md#getRecipientOfSending) | **GET** /sendings/{sending_id}/recipients/{recipient_id} | Détail d&#39;un destinataire |
| [**patchRecipientOfSending()**](DestinatairesApi.md#patchRecipientOfSending) | **PATCH** /sendings/{sending_id}/recipients/{recipient_id} | Modification partielle d&#39;un destinataire |


## `createRecipientOfSending()`

```php
createRecipientOfSending($sending_id, $recipient_creation): \OpenAPI\Client\Model\RecipientResponse
```

Ajout d'un destinataire à l'envoi

Permet d'ajouter un destinataire à l'envoi. Le nombre de destinataires est limité à 5000.   Chaque ligne d’adresse doit contenir au plus 38 caractères. La ligne d’adresse 1 ou 2 doit être définie. La ligne d’adresse 6 doit être définie. Pour les adresses françaises, la ligne d’adresse 6 doit contenir un code postal sur 5 chiffres, suivi d’un espace, suivi d’une ville.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: oAuth2PasswordProduction
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oAuth2PasswordSandbox
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\DestinatairesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sending_id = 'sending_id_example'; // string | Identifiant d'un envoi
$recipient_creation = new \OpenAPI\Client\Model\RecipientCreation(); // \OpenAPI\Client\Model\RecipientCreation

try {
    $result = $apiInstance->createRecipientOfSending($sending_id, $recipient_creation);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DestinatairesApi->createRecipientOfSending: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sending_id** | **string**| Identifiant d&#39;un envoi | |
| **recipient_creation** | [**\OpenAPI\Client\Model\RecipientCreation**](../Model/RecipientCreation.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\RecipientResponse**](../Model/RecipientResponse.md)

### Authorization

[oAuth2PasswordProduction](../../README.md#oAuth2PasswordProduction), [oAuth2PasswordSandbox](../../README.md#oAuth2PasswordSandbox), [bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `deleteAllRecipientsOfSending()`

```php
deleteAllRecipientsOfSending($sending_id)
```

Suppression de tous les destinataires

Permet de supprimer tous les destinataires d'un envoi.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: oAuth2PasswordProduction
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oAuth2PasswordSandbox
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\DestinatairesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sending_id = 'sending_id_example'; // string | Identifiant d'un envoi

try {
    $apiInstance->deleteAllRecipientsOfSending($sending_id);
} catch (Exception $e) {
    echo 'Exception when calling DestinatairesApi->deleteAllRecipientsOfSending: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sending_id** | **string**| Identifiant d&#39;un envoi | |

### Return type

void (empty response body)

### Authorization

[oAuth2PasswordProduction](../../README.md#oAuth2PasswordProduction), [oAuth2PasswordSandbox](../../README.md#oAuth2PasswordSandbox), [bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `deleteRecipientOfSending()`

```php
deleteRecipientOfSending($sending_id, $recipient_id)
```

Suppression d'un destinataire

Permet de supprimer un destinataire d'un envoi.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: oAuth2PasswordProduction
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oAuth2PasswordSandbox
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\DestinatairesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sending_id = 'sending_id_example'; // string | Identifiant d'un envoi
$recipient_id = 'recipient_id_example'; // string | Identifiant du destinataire

try {
    $apiInstance->deleteRecipientOfSending($sending_id, $recipient_id);
} catch (Exception $e) {
    echo 'Exception when calling DestinatairesApi->deleteRecipientOfSending: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sending_id** | **string**| Identifiant d&#39;un envoi | |
| **recipient_id** | **string**| Identifiant du destinataire | |

### Return type

void (empty response body)

### Authorization

[oAuth2PasswordProduction](../../README.md#oAuth2PasswordProduction), [oAuth2PasswordSandbox](../../README.md#oAuth2PasswordSandbox), [bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `downloadAcknowledgementOfReceiptOfRecipientOfSending()`

```php
downloadAcknowledgementOfReceiptOfRecipientOfSending($sending_id, $recipient_id): \SplFileObject
```

Télécharger l'avis de réception archivé du destinataire

Cette API permet de télécharger au format PDF l'avis de réception archivé du destinataire.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: oAuth2PasswordProduction
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oAuth2PasswordSandbox
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\DestinatairesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sending_id = 'sending_id_example'; // string | Identifiant d'un envoi
$recipient_id = 'recipient_id_example'; // string | Identifiant du destinataire

try {
    $result = $apiInstance->downloadAcknowledgementOfReceiptOfRecipientOfSending($sending_id, $recipient_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DestinatairesApi->downloadAcknowledgementOfReceiptOfRecipientOfSending: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sending_id** | **string**| Identifiant d&#39;un envoi | |
| **recipient_id** | **string**| Identifiant du destinataire | |

### Return type

**\SplFileObject**

### Authorization

[oAuth2PasswordProduction](../../README.md#oAuth2PasswordProduction), [oAuth2PasswordSandbox](../../README.md#oAuth2PasswordSandbox), [bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/pdf`, `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `downloadArchivedUndeliveredMailOfRecipientOfSending()`

```php
downloadArchivedUndeliveredMailOfRecipientOfSending($sending_id, $recipient_id): \SplFileObject
```

Téléchargement de l'image du pli retourné

Permet de télécharger au format PDF l'image du pli retourné.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: oAuth2PasswordProduction
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oAuth2PasswordSandbox
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\DestinatairesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sending_id = 'sending_id_example'; // string | Identifiant d'un envoi
$recipient_id = 'recipient_id_example'; // string | Identifiant du destinataire

try {
    $result = $apiInstance->downloadArchivedUndeliveredMailOfRecipientOfSending($sending_id, $recipient_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DestinatairesApi->downloadArchivedUndeliveredMailOfRecipientOfSending: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sending_id** | **string**| Identifiant d&#39;un envoi | |
| **recipient_id** | **string**| Identifiant du destinataire | |

### Return type

**\SplFileObject**

### Authorization

[oAuth2PasswordProduction](../../README.md#oAuth2PasswordProduction), [oAuth2PasswordSandbox](../../README.md#oAuth2PasswordSandbox), [bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/pdf`, `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `downloadContentProofOfRecipientOfSending()`

```php
downloadContentProofOfRecipientOfSending($sending_id, $recipient_id): \SplFileObject
```

Télécharger la preuve de contenu du destinataire

Cette API permet de télécharger au format PDF la preuve de contenu archivée du destinataire.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: oAuth2PasswordProduction
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oAuth2PasswordSandbox
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\DestinatairesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sending_id = 'sending_id_example'; // string | Identifiant d'un envoi
$recipient_id = 'recipient_id_example'; // string | Identifiant du destinataire

try {
    $result = $apiInstance->downloadContentProofOfRecipientOfSending($sending_id, $recipient_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DestinatairesApi->downloadContentProofOfRecipientOfSending: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sending_id** | **string**| Identifiant d&#39;un envoi | |
| **recipient_id** | **string**| Identifiant du destinataire | |

### Return type

**\SplFileObject**

### Authorization

[oAuth2PasswordProduction](../../README.md#oAuth2PasswordProduction), [oAuth2PasswordSandbox](../../README.md#oAuth2PasswordSandbox), [bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/pdf`, `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `downloadDepositProofOfRecipientOfSending()`

```php
downloadDepositProofOfRecipientOfSending($sending_id, $recipient_id): \SplFileObject
```

Télécharger la preuve de dépôt du destinataire

Cette API permet de télécharger au format PDF la preuve de dépôt archivée du destinataire.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: oAuth2PasswordProduction
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oAuth2PasswordSandbox
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\DestinatairesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sending_id = 'sending_id_example'; // string | Identifiant d'un envoi
$recipient_id = 'recipient_id_example'; // string | Identifiant du destinataire

try {
    $result = $apiInstance->downloadDepositProofOfRecipientOfSending($sending_id, $recipient_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DestinatairesApi->downloadDepositProofOfRecipientOfSending: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sending_id** | **string**| Identifiant d&#39;un envoi | |
| **recipient_id** | **string**| Identifiant du destinataire | |

### Return type

**\SplFileObject**

### Authorization

[oAuth2PasswordProduction](../../README.md#oAuth2PasswordProduction), [oAuth2PasswordSandbox](../../README.md#oAuth2PasswordSandbox), [bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/pdf`, `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `downloadEmbeddedDocumentOfRecipientOfSending()`

```php
downloadEmbeddedDocumentOfRecipientOfSending($sending_id, $recipient_id): \SplFileObject
```

Télécharger le document contenu dans la preuve de contenu

Cette API permet de télécharger au format PDF le document contenu dans la preuve de contenu archivée du destinataire.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: oAuth2PasswordProduction
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oAuth2PasswordSandbox
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\DestinatairesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sending_id = 'sending_id_example'; // string | Identifiant d'un envoi
$recipient_id = 'recipient_id_example'; // string | Identifiant du destinataire

try {
    $result = $apiInstance->downloadEmbeddedDocumentOfRecipientOfSending($sending_id, $recipient_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DestinatairesApi->downloadEmbeddedDocumentOfRecipientOfSending: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sending_id** | **string**| Identifiant d&#39;un envoi | |
| **recipient_id** | **string**| Identifiant du destinataire | |

### Return type

**\SplFileObject**

### Authorization

[oAuth2PasswordProduction](../../README.md#oAuth2PasswordProduction), [oAuth2PasswordSandbox](../../README.md#oAuth2PasswordSandbox), [bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/pdf`, `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getAllRecipientsOfSending()`

```php
getAllRecipientsOfSending($sending_id, $start_index, $count): \OpenAPI\Client\Model\RecipientsResponse
```

Liste des destinataires d'un envoi

Permet de récupérer la liste des destinataires d'un envoi. Cette liste peut être paginée. Par défaut, la pagination est de 50 résultats. Elle peut atteindre 500 au maximum.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: oAuth2PasswordProduction
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oAuth2PasswordSandbox
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\DestinatairesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sending_id = 'sending_id_example'; // string | Identifiant d'un envoi
$start_index = 1; // int | Le premier élément à retourner
$count = 50; // int | Le nombre d'élément à retourner

try {
    $result = $apiInstance->getAllRecipientsOfSending($sending_id, $start_index, $count);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DestinatairesApi->getAllRecipientsOfSending: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sending_id** | **string**| Identifiant d&#39;un envoi | |
| **start_index** | **int**| Le premier élément à retourner | [optional] [default to 1] |
| **count** | **int**| Le nombre d&#39;élément à retourner | [optional] [default to 50] |

### Return type

[**\OpenAPI\Client\Model\RecipientsResponse**](../Model/RecipientsResponse.md)

### Authorization

[oAuth2PasswordProduction](../../README.md#oAuth2PasswordProduction), [oAuth2PasswordSandbox](../../README.md#oAuth2PasswordSandbox), [bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getRecipientOfSending()`

```php
getRecipientOfSending($sending_id, $recipient_id): \OpenAPI\Client\Model\RecipientResponse
```

Détail d'un destinataire

Permet d'obtenir le détail d'un destinataire d'un envoi.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: oAuth2PasswordProduction
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oAuth2PasswordSandbox
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\DestinatairesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sending_id = 'sending_id_example'; // string | Identifiant d'un envoi
$recipient_id = 'recipient_id_example'; // string | Identifiant du destinataire

try {
    $result = $apiInstance->getRecipientOfSending($sending_id, $recipient_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DestinatairesApi->getRecipientOfSending: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sending_id** | **string**| Identifiant d&#39;un envoi | |
| **recipient_id** | **string**| Identifiant du destinataire | |

### Return type

[**\OpenAPI\Client\Model\RecipientResponse**](../Model/RecipientResponse.md)

### Authorization

[oAuth2PasswordProduction](../../README.md#oAuth2PasswordProduction), [oAuth2PasswordSandbox](../../README.md#oAuth2PasswordSandbox), [bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `patchRecipientOfSending()`

```php
patchRecipientOfSending($sending_id, $recipient_id, $recipient_update): \OpenAPI\Client\Model\RecipientResponse
```

Modification partielle d'un destinataire

Permet de modifier partiellement un destinataire

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: oAuth2PasswordProduction
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oAuth2PasswordSandbox
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\DestinatairesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sending_id = 'sending_id_example'; // string | Identifiant d'un envoi
$recipient_id = 'recipient_id_example'; // string | Identifiant du destinataire
$recipient_update = new \OpenAPI\Client\Model\RecipientUpdate(); // \OpenAPI\Client\Model\RecipientUpdate

try {
    $result = $apiInstance->patchRecipientOfSending($sending_id, $recipient_id, $recipient_update);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DestinatairesApi->patchRecipientOfSending: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sending_id** | **string**| Identifiant d&#39;un envoi | |
| **recipient_id** | **string**| Identifiant du destinataire | |
| **recipient_update** | [**\OpenAPI\Client\Model\RecipientUpdate**](../Model/RecipientUpdate.md)|  | |

### Return type

[**\OpenAPI\Client\Model\RecipientResponse**](../Model/RecipientResponse.md)

### Authorization

[oAuth2PasswordProduction](../../README.md#oAuth2PasswordProduction), [oAuth2PasswordSandbox](../../README.md#oAuth2PasswordSandbox), [bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
