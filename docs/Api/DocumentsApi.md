# OpenAPI\Client\DocumentsApi

All URIs are relative to https://api.sandbox.maileva.net/registered_mail/v4, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**deleteDocumentOfSending()**](DocumentsApi.md#deleteDocumentOfSending) | **DELETE** /sendings/{sending_id}/documents/{document_id} | Suppression d&#39;un document |
| [**getAllDocumentsOfSending()**](DocumentsApi.md#getAllDocumentsOfSending) | **GET** /sendings/{sending_id}/documents | Liste des documents d&#39;un envoi |
| [**getDocumentOfSending()**](DocumentsApi.md#getDocumentOfSending) | **GET** /sendings/{sending_id}/documents/{document_id} | Détail d&#39;un document |
| [**postDocumentOfSending()**](DocumentsApi.md#postDocumentOfSending) | **POST** /sendings/{sending_id}/documents | Ajout d&#39;un document à l&#39;envoi. |
| [**setDocumentPriorityOfSending()**](DocumentsApi.md#setDocumentPriorityOfSending) | **POST** /sendings/{sending_id}/documents/{document_id}/set_priority | Classement des documents |


## `deleteDocumentOfSending()`

```php
deleteDocumentOfSending($sending_id, $document_id)
```

Suppression d'un document

Permet de supprimer un document d'un envoi.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: oAuth2PasswordProduction
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oAuth2PasswordSandbox
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\DocumentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sending_id = 'sending_id_example'; // string | Identifiant d'un envoi
$document_id = 'document_id_example'; // string | Identifiant du document

try {
    $apiInstance->deleteDocumentOfSending($sending_id, $document_id);
} catch (Exception $e) {
    echo 'Exception when calling DocumentsApi->deleteDocumentOfSending: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sending_id** | **string**| Identifiant d&#39;un envoi | |
| **document_id** | **string**| Identifiant du document | |

### Return type

void (empty response body)

### Authorization

[oAuth2PasswordProduction](../../README.md#oAuth2PasswordProduction), [oAuth2PasswordSandbox](../../README.md#oAuth2PasswordSandbox), [bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getAllDocumentsOfSending()`

```php
getAllDocumentsOfSending($sending_id, $start_index, $count): \OpenAPI\Client\Model\DocumentsResponse
```

Liste des documents d'un envoi

Permet de récupérer la liste des documents associés à l'envoi. La liste des documents d'un envoi peut être paginée. Par défaut et au maximum, la pagination est de 30 résultats.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: oAuth2PasswordProduction
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oAuth2PasswordSandbox
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\DocumentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sending_id = 'sending_id_example'; // string | Identifiant d'un envoi
$start_index = 1; // int | Le premier élément à retourner
$count = 30; // float | Le nombre d'éléments à retourner

try {
    $result = $apiInstance->getAllDocumentsOfSending($sending_id, $start_index, $count);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DocumentsApi->getAllDocumentsOfSending: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sending_id** | **string**| Identifiant d&#39;un envoi | |
| **start_index** | **int**| Le premier élément à retourner | [optional] [default to 1] |
| **count** | **float**| Le nombre d&#39;éléments à retourner | [optional] [default to 30] |

### Return type

[**\OpenAPI\Client\Model\DocumentsResponse**](../Model/DocumentsResponse.md)

### Authorization

[oAuth2PasswordProduction](../../README.md#oAuth2PasswordProduction), [oAuth2PasswordSandbox](../../README.md#oAuth2PasswordSandbox), [bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getDocumentOfSending()`

```php
getDocumentOfSending($sending_id, $document_id): \OpenAPI\Client\Model\DocumentResponse
```

Détail d'un document

Permet de récupérer le détail d'un document utilisé lors de l'envoi.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: oAuth2PasswordProduction
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oAuth2PasswordSandbox
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\DocumentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sending_id = 'sending_id_example'; // string | Identifiant d'un envoi
$document_id = 'document_id_example'; // string | Identifiant du document

try {
    $result = $apiInstance->getDocumentOfSending($sending_id, $document_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DocumentsApi->getDocumentOfSending: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sending_id** | **string**| Identifiant d&#39;un envoi | |
| **document_id** | **string**| Identifiant du document | |

### Return type

[**\OpenAPI\Client\Model\DocumentResponse**](../Model/DocumentResponse.md)

### Authorization

[oAuth2PasswordProduction](../../README.md#oAuth2PasswordProduction), [oAuth2PasswordSandbox](../../README.md#oAuth2PasswordSandbox), [bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `postDocumentOfSending()`

```php
postDocumentOfSending($sending_id, $document, $metadata): \OpenAPI\Client\Model\DocumentResponse
```

Ajout d'un document à l'envoi.

Permet d'ajouter un document à l'envoi. Les types de documents autorisés sont :   - Adobe (.pdf)   - Word (.doc, .docx et .rtf)   - Texte (.txt)   - Excel (.xls, .xlsx)  Le document ajouté ne doit pas dépasser 20 Mo. Le nombre total de documents est limité à 30 par envoi. Le document doit être transmis en mutipart ainsi que la metadata. La metadata est constituée de *priority* (permet de définir l'ordre d'impression des documents) et de *name* (permet de donner un nom au fichier). La première page du document est positionnée systématiquement sur le recto de la feuille.  Le  nombre de feuilles d’un envoi ne doit pas dépasser la capacité de l’enveloppe    - Enveloppe grand format C4 (210x300 mm, Double fenêtre) : 44 feuilles maximum (hors feuille porte-adresse, enveloppe retour incluse)    - Enveloppe petit format DL (114x229 mm Simple ou Double-fenêtre) : 5 feuilles maximum (feuille porte-adresse et enveloppe retour incluses)

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: oAuth2PasswordProduction
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oAuth2PasswordSandbox
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\DocumentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sending_id = 'sending_id_example'; // string | Identifiant d'un envoi
$document = "/path/to/file.txt"; // \SplFileObject
$metadata = new \OpenAPI\Client\Model\DocumentMetadata(); // \OpenAPI\Client\Model\DocumentMetadata

try {
    $result = $apiInstance->postDocumentOfSending($sending_id, $document, $metadata);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DocumentsApi->postDocumentOfSending: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sending_id** | **string**| Identifiant d&#39;un envoi | |
| **document** | **\SplFileObject****\SplFileObject**|  | [optional] |
| **metadata** | [**\OpenAPI\Client\Model\DocumentMetadata**](../Model/DocumentMetadata.md)|  | [optional] |

### Return type

[**\OpenAPI\Client\Model\DocumentResponse**](../Model/DocumentResponse.md)

### Authorization

[oAuth2PasswordProduction](../../README.md#oAuth2PasswordProduction), [oAuth2PasswordSandbox](../../README.md#oAuth2PasswordSandbox), [bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: `multipart/form-data`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `setDocumentPriorityOfSending()`

```php
setDocumentPriorityOfSending($sending_id, $document_id, $priority)
```

Classement des documents

Permet d'ordonner les documents d'un envoi.  Les documents seront imprimés et mis sous pli dans l'ordre choisi.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure OAuth2 access token for authorization: oAuth2PasswordProduction
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oAuth2PasswordSandbox
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\DocumentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sending_id = 'sending_id_example'; // string | Identifiant d'un envoi
$document_id = 'document_id_example'; // string | Identifiant du document
$priority = new \OpenAPI\Client\Model\Priority(); // \OpenAPI\Client\Model\Priority

try {
    $apiInstance->setDocumentPriorityOfSending($sending_id, $document_id, $priority);
} catch (Exception $e) {
    echo 'Exception when calling DocumentsApi->setDocumentPriorityOfSending: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sending_id** | **string**| Identifiant d&#39;un envoi | |
| **document_id** | **string**| Identifiant du document | |
| **priority** | [**\OpenAPI\Client\Model\Priority**](../Model/Priority.md)|  | [optional] |

### Return type

void (empty response body)

### Authorization

[oAuth2PasswordProduction](../../README.md#oAuth2PasswordProduction), [oAuth2PasswordSandbox](../../README.md#oAuth2PasswordSandbox), [bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
