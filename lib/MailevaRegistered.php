<?php

namespace MailevaRegistered\Client;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Query;
use GuzzleHttp\Psr7\Request;
use MailevaRegistered\Client\Api\EnvoiApi;
use MailevaRegistered\Client\Api\DocumentsApi;
use MailevaRegistered\Client\Api\DestinatairesApi;
use MailevaRegistered\Client\Api\PreuveDeDptGlobaleApi;
use MailevaRegistered\Client\Configuration;

/**
 * Description of Maileva
 *
 * @author jp10
 */
class MailevaRegistered
{
    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * @var Configuration
     */
    protected $config;

    public function __construct(
        $username,
        $password,
        $client_id,
        $client_secret,
        ClientInterface $client,
        Configuration $config
    )
    {
        $this->client = $client;
        $this->config = $config;

        $this->config->setUsername($username);
        $this->config->setPassword($password);

        $this->connexion($client_id, $client_secret);
    }

    protected function connexion($client_id, $client_secret)
    {
        $response = $this->client->send(
            new Request(
                'POST',
                $this->config->getAuth(),
                [
                    'Cache-Control' => 'no-cache',
                    'Content-Type' => 'application/x-www-form-urlencoded'
                ],
                Query::build(
                    [
                        'username' => $this->config->getUsername(),
                        'password' => $this->config->getPassword(),
                        'grant_type' => 'password',
                        'client_id' => $client_id,
                        'client_secret' => $client_secret
                    ]
                )
            )
        )->getBody()->getContents();

        $this->config->setAccessToken(
            json_decode($response)->access_token
        );
    }

    /**
     *
     * @return EnvoiApi
     */
    public function envoi()
    {
        return new EnvoiApi(
            $this->client,
            $this->config
        );
    }

    /**
     *
     * @return DocumentsApi
     */
    public function documents()
    {
        return new DocumentsApi(
            $this->client,
            $this->config
        );
    }

    /**
     *
     * @return DestinatairesApi
     */
    public function destinataires()
    {
        return new DestinatairesApi(
            $this->client,
            $this->config
        );
    }

    /**
     *
     * @return PreuveDeDptGlobaleApi
     */
    public function preuves()
    {
        return new PreuveDeDptGlobaleApi(
            $this->client,
            $this->config
        );
    }
}