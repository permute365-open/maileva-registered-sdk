<?php
/**
 * RecipientStatus
 *
 * PHP version 7.4
 *
 * @category Class
 * @package  MailevaRegistered\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Maileva / Envoi et Suivi de Lettres Recommandées En Ligne distribuées par le facteur
 *
 * API pour envoyer et suivre des Lettres Recommandées En Ligne distribuées par le facteur  Elle comprend les fonctions clés pour :   - créer un envoi,  - ajouter des documents et des destinataires,  - choisir ses options,  - suivre la production.  Pour connaître les notifications (webhooks) concernées par cette API, consultez la documentation de l'API <a href=\"/developpeur/notification_center\">notification_center</a>.
 *
 * The version of the OpenAPI document: 4.4
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 7.1.0-SNAPSHOT
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace MailevaRegistered\Client\Model;
use \MailevaRegistered\Client\ObjectSerializer;

/**
 * RecipientStatus Class Doc Comment
 *
 * @category Class
 * @description Statut du destinataire. &lt;table&gt;   &lt;tr&gt;     &lt;td&gt;DRAFT&lt;/td&gt;     &lt;td&gt;le destinataire a été ajouté à l&#39;envoi qui est au statut draft&lt;/td&gt;   &lt;/tr&gt;   &lt;tr&gt;     &lt;td&gt;PENDING&lt;/td&gt;     &lt;td&gt;le destinataire n&#39;a été ni traité, ni rejeté&lt;/td&gt;   &lt;/tr&gt;   &lt;tr&gt;     &lt;td&gt;BLOCKED&lt;/td&gt;     &lt;td&gt;Envoi bloqué&lt;/td&gt;   &lt;/tr&gt;   &lt;tr&gt;     &lt;td&gt;ACCEPTED&lt;/td&gt;     &lt;td&gt;Validé&lt;/td&gt;   &lt;/tr&gt;   &lt;tr&gt;     &lt;td&gt;PROCESSED&lt;/td&gt;     &lt;td&gt;le destinataire a été mis sous pli (liasse appairée si recommandée)&lt;/td&gt;   &lt;/tr&gt;   &lt;tr&gt;     &lt;td&gt;REJECTED&lt;/td&gt;     &lt;td&gt;le destinataire n&#39;est pas valide&lt;/td&gt;   &lt;/tr&gt;   &lt;tr&gt;     &lt;td&gt;SENDING_REJECTED&lt;/td&gt;     &lt;td&gt;Envoi rejeté&lt;/td&gt;   &lt;/tr&gt;   &lt;tr&gt;     &lt;td&gt;CANCELED&lt;/td&gt;     &lt;td&gt;Annulé&lt;/td&gt;   &lt;/tr&gt;   &lt;tr&gt;     &lt;td&gt;PREPARING&lt;/td&gt;     &lt;td&gt;En préparation&lt;/td&gt;   &lt;/tr&gt; &lt;/table&gt;
 * @package  MailevaRegistered\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class RecipientStatus
{
    /**
     * Possible values of this enum
     */
    public const DRAFT = 'DRAFT';

    public const PENDING = 'PENDING';

    public const BLOCKED = 'BLOCKED';

    public const ACCEPTED = 'ACCEPTED';

    public const SENDING_REJECTED = 'SENDING_REJECTED';

    public const CANCELED = 'CANCELED';

    public const PREPARING = 'PREPARING';

    public const PROCESSED = 'PROCESSED';

    public const REJECTED = 'REJECTED';

    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public static function getAllowableEnumValues()
    {
        return [
            self::DRAFT,
            self::PENDING,
            self::BLOCKED,
            self::ACCEPTED,
            self::SENDING_REJECTED,
            self::CANCELED,
            self::PREPARING,
            self::PROCESSED,
            self::REJECTED
        ];
    }
}


