<?php
/**
 * RecipientsCounts
 *
 * PHP version 7.4
 *
 * @category Class
 * @package  MailevaRegistered\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Maileva / Envoi et Suivi de Lettres Recommandées En Ligne distribuées par le facteur
 *
 * API pour envoyer et suivre des Lettres Recommandées En Ligne distribuées par le facteur  Elle comprend les fonctions clés pour :   - créer un envoi,  - ajouter des documents et des destinataires,  - choisir ses options,  - suivre la production.  Pour connaître les notifications (webhooks) concernées par cette API, consultez la documentation de l'API <a href=\"/developpeur/notification_center\">notification_center</a>.
 *
 * The version of the OpenAPI document: 4.4
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 7.1.0-SNAPSHOT
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace MailevaRegistered\Client\Model;

use \ArrayAccess;
use \MailevaRegistered\Client\ObjectSerializer;

/**
 * RecipientsCounts Class Doc Comment
 *
 * @category Class
 * @package  MailevaRegistered\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 * @implements \ArrayAccess<string, mixed>
 */
class RecipientsCounts implements ModelInterface, ArrayAccess, \JsonSerializable
{
    public const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $openAPIModelName = 'recipients_counts';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPITypes = [
        'draft' => 'int',
        'pending' => 'int',
        'blocked' => 'int',
        'accepted' => 'int',
        'processed' => 'int',
        'sending_rejected' => 'int',
        'canceled' => 'int',
        'preparing' => 'int',
        'rejected' => 'int',
        'total' => 'int'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      * @phpstan-var array<string, string|null>
      * @psalm-var array<string, string|null>
      */
    protected static $openAPIFormats = [
        'draft' => null,
        'pending' => null,
        'blocked' => null,
        'accepted' => null,
        'processed' => null,
        'sending_rejected' => null,
        'canceled' => null,
        'preparing' => null,
        'rejected' => null,
        'total' => null
    ];

    /**
      * Array of nullable properties. Used for (de)serialization
      *
      * @var boolean[]
      */
    protected static array $openAPINullables = [
        'draft' => false,
		'pending' => false,
		'blocked' => false,
		'accepted' => false,
		'processed' => false,
		'sending_rejected' => false,
		'canceled' => false,
		'preparing' => false,
		'rejected' => false,
		'total' => false
    ];

    /**
      * If a nullable field gets set to null, insert it here
      *
      * @var boolean[]
      */
    protected array $openAPINullablesSetToNull = [];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPITypes()
    {
        return self::$openAPITypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPIFormats()
    {
        return self::$openAPIFormats;
    }

    /**
     * Array of nullable properties
     *
     * @return array
     */
    protected static function openAPINullables(): array
    {
        return self::$openAPINullables;
    }

    /**
     * Array of nullable field names deliberately set to null
     *
     * @return boolean[]
     */
    private function getOpenAPINullablesSetToNull(): array
    {
        return $this->openAPINullablesSetToNull;
    }

    /**
     * Setter - Array of nullable field names deliberately set to null
     *
     * @param boolean[] $openAPINullablesSetToNull
     */
    private function setOpenAPINullablesSetToNull(array $openAPINullablesSetToNull): void
    {
        $this->openAPINullablesSetToNull = $openAPINullablesSetToNull;
    }

    /**
     * Checks if a property is nullable
     *
     * @param string $property
     * @return bool
     */
    public static function isNullable(string $property): bool
    {
        return self::openAPINullables()[$property] ?? false;
    }

    /**
     * Checks if a nullable property is set to null.
     *
     * @param string $property
     * @return bool
     */
    public function isNullableSetToNull(string $property): bool
    {
        return in_array($property, $this->getOpenAPINullablesSetToNull(), true);
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'draft' => 'draft',
        'pending' => 'pending',
        'blocked' => 'blocked',
        'accepted' => 'accepted',
        'processed' => 'processed',
        'sending_rejected' => 'sending_rejected',
        'canceled' => 'canceled',
        'preparing' => 'preparing',
        'rejected' => 'rejected',
        'total' => 'total'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'draft' => 'setDraft',
        'pending' => 'setPending',
        'blocked' => 'setBlocked',
        'accepted' => 'setAccepted',
        'processed' => 'setProcessed',
        'sending_rejected' => 'setSendingRejected',
        'canceled' => 'setCanceled',
        'preparing' => 'setPreparing',
        'rejected' => 'setRejected',
        'total' => 'setTotal'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'draft' => 'getDraft',
        'pending' => 'getPending',
        'blocked' => 'getBlocked',
        'accepted' => 'getAccepted',
        'processed' => 'getProcessed',
        'sending_rejected' => 'getSendingRejected',
        'canceled' => 'getCanceled',
        'preparing' => 'getPreparing',
        'rejected' => 'getRejected',
        'total' => 'getTotal'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$openAPIModelName;
    }


    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->setIfExists('draft', $data ?? [], null);
        $this->setIfExists('pending', $data ?? [], null);
        $this->setIfExists('blocked', $data ?? [], null);
        $this->setIfExists('accepted', $data ?? [], null);
        $this->setIfExists('processed', $data ?? [], null);
        $this->setIfExists('sending_rejected', $data ?? [], null);
        $this->setIfExists('canceled', $data ?? [], null);
        $this->setIfExists('preparing', $data ?? [], null);
        $this->setIfExists('rejected', $data ?? [], null);
        $this->setIfExists('total', $data ?? [], null);
    }

    /**
    * Sets $this->container[$variableName] to the given data or to the given default Value; if $variableName
    * is nullable and its value is set to null in the $fields array, then mark it as "set to null" in the
    * $this->openAPINullablesSetToNull array
    *
    * @param string $variableName
    * @param array  $fields
    * @param mixed  $defaultValue
    */
    private function setIfExists(string $variableName, array $fields, $defaultValue): void
    {
        if (self::isNullable($variableName) && array_key_exists($variableName, $fields) && is_null($fields[$variableName])) {
            $this->openAPINullablesSetToNull[] = $variableName;
        }

        $this->container[$variableName] = $fields[$variableName] ?? $defaultValue;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets draft
     *
     * @return int|null
     */
    public function getDraft()
    {
        return $this->container['draft'];
    }

    /**
     * Sets draft
     *
     * @param int|null $draft Nombre de destinataires au statut DRAFT
     *
     * @return self
     */
    public function setDraft($draft)
    {
        if (is_null($draft)) {
            throw new \InvalidArgumentException('non-nullable draft cannot be null');
        }
        $this->container['draft'] = $draft;

        return $this;
    }

    /**
     * Gets pending
     *
     * @return int|null
     */
    public function getPending()
    {
        return $this->container['pending'];
    }

    /**
     * Sets pending
     *
     * @param int|null $pending Nombre de destinataires au statut PENDING
     *
     * @return self
     */
    public function setPending($pending)
    {
        if (is_null($pending)) {
            throw new \InvalidArgumentException('non-nullable pending cannot be null');
        }
        $this->container['pending'] = $pending;

        return $this;
    }

    /**
     * Gets blocked
     *
     * @return int|null
     */
    public function getBlocked()
    {
        return $this->container['blocked'];
    }

    /**
     * Sets blocked
     *
     * @param int|null $blocked Nombre de destinataires au statut BLOCKED
     *
     * @return self
     */
    public function setBlocked($blocked)
    {
        if (is_null($blocked)) {
            throw new \InvalidArgumentException('non-nullable blocked cannot be null');
        }
        $this->container['blocked'] = $blocked;

        return $this;
    }

    /**
     * Gets accepted
     *
     * @return int|null
     */
    public function getAccepted()
    {
        return $this->container['accepted'];
    }

    /**
     * Sets accepted
     *
     * @param int|null $accepted Nombre de destinataires au statut ACCEPTED
     *
     * @return self
     */
    public function setAccepted($accepted)
    {
        if (is_null($accepted)) {
            throw new \InvalidArgumentException('non-nullable accepted cannot be null');
        }
        $this->container['accepted'] = $accepted;

        return $this;
    }

    /**
     * Gets processed
     *
     * @return int|null
     */
    public function getProcessed()
    {
        return $this->container['processed'];
    }

    /**
     * Sets processed
     *
     * @param int|null $processed Nombre de destinataires au statut PROCESSED
     *
     * @return self
     */
    public function setProcessed($processed)
    {
        if (is_null($processed)) {
            throw new \InvalidArgumentException('non-nullable processed cannot be null');
        }
        $this->container['processed'] = $processed;

        return $this;
    }

    /**
     * Gets sending_rejected
     *
     * @return int|null
     */
    public function getSendingRejected()
    {
        return $this->container['sending_rejected'];
    }

    /**
     * Sets sending_rejected
     *
     * @param int|null $sending_rejected Nombre de destinataires au statut SENDING_REJECTED
     *
     * @return self
     */
    public function setSendingRejected($sending_rejected)
    {
        if (is_null($sending_rejected)) {
            throw new \InvalidArgumentException('non-nullable sending_rejected cannot be null');
        }
        $this->container['sending_rejected'] = $sending_rejected;

        return $this;
    }

    /**
     * Gets canceled
     *
     * @return int|null
     */
    public function getCanceled()
    {
        return $this->container['canceled'];
    }

    /**
     * Sets canceled
     *
     * @param int|null $canceled Nombre de destinataires au statut CANCELED
     *
     * @return self
     */
    public function setCanceled($canceled)
    {
        if (is_null($canceled)) {
            throw new \InvalidArgumentException('non-nullable canceled cannot be null');
        }
        $this->container['canceled'] = $canceled;

        return $this;
    }

    /**
     * Gets preparing
     *
     * @return int|null
     */
    public function getPreparing()
    {
        return $this->container['preparing'];
    }

    /**
     * Sets preparing
     *
     * @param int|null $preparing Nombre de destinataires au statut PREPARING
     *
     * @return self
     */
    public function setPreparing($preparing)
    {
        if (is_null($preparing)) {
            throw new \InvalidArgumentException('non-nullable preparing cannot be null');
        }
        $this->container['preparing'] = $preparing;

        return $this;
    }

    /**
     * Gets rejected
     *
     * @return int|null
     */
    public function getRejected()
    {
        return $this->container['rejected'];
    }

    /**
     * Sets rejected
     *
     * @param int|null $rejected Nombre de destinataires au statut REJECTED
     *
     * @return self
     */
    public function setRejected($rejected)
    {
        if (is_null($rejected)) {
            throw new \InvalidArgumentException('non-nullable rejected cannot be null');
        }
        $this->container['rejected'] = $rejected;

        return $this;
    }

    /**
     * Gets total
     *
     * @return int|null
     */
    public function getTotal()
    {
        return $this->container['total'];
    }

    /**
     * Sets total
     *
     * @param int|null $total Nombre de destinataires total
     *
     * @return self
     */
    public function setTotal($total)
    {
        if (is_null($total)) {
            throw new \InvalidArgumentException('non-nullable total cannot be null');
        }
        $this->container['total'] = $total;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset): bool
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed|null
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return $this->container[$offset] ?? null;
    }

    /**
     * Sets value based on offset.
     *
     * @param int|null $offset Offset
     * @param mixed    $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value): void
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset): void
    {
        unset($this->container[$offset]);
    }

    /**
     * Serializes the object to a value that can be serialized natively by json_encode().
     * @link https://www.php.net/manual/en/jsonserializable.jsonserialize.php
     *
     * @return mixed Returns data which can be serialized by json_encode(), which is a value
     * of any type other than a resource.
     */
    #[\ReturnTypeWillChange]
    public function jsonSerialize()
    {
       return ObjectSerializer::sanitizeForSerialization($this);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        return json_encode(
            ObjectSerializer::sanitizeForSerialization($this),
            JSON_PRETTY_PRINT
        );
    }

    /**
     * Gets a header-safe presentation of the object
     *
     * @return string
     */
    public function toHeaderValue()
    {
        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


