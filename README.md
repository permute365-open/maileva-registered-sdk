# Maileva-registered-sdk

API pour envoyer et suivre des Lettres Recommandées En Ligne distribuées par le facteur

Elle comprend les fonctions clés pour :

 - créer un envoi,
 - ajouter des documents et des destinataires,
 - choisir ses options,
 - suivre la production.

Pour connaître les notifications (webhooks) concernées par cette API, consultez la documentation de l'API notification_center.


## Installation & Usage

### Requirements

PHP 7.4 and later.
Should also work with PHP 8.0.

### Composer

To install the bindings via [Composer](https://getcomposer.org/), run the following:

```json
composer require jp10/maileva-registered-sdk
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



// Configure OAuth2 access token for authorization: oAuth2PasswordProduction
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure OAuth2 access token for authorization: oAuth2PasswordSandbox
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

// Configure Bearer (JWT) authorization: bearerAuth
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new OpenAPI\Client\Api\DestinatairesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sending_id = 'sending_id_example'; // string | Identifiant d'un envoi
$recipient_creation = new \OpenAPI\Client\Model\RecipientCreation(); // \OpenAPI\Client\Model\RecipientCreation

try {
    $result = $apiInstance->createRecipientOfSending($sending_id, $recipient_creation);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DestinatairesApi->createRecipientOfSending: ', $e->getMessage(), PHP_EOL;
}

```

## API Endpoints

All URIs are relative to *https://api.sandbox.maileva.net/registered_mail/v4*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*DestinatairesApi* | [**createRecipientOfSending**](docs/Api/DestinatairesApi.md#createrecipientofsending) | **POST** /sendings/{sending_id}/recipients | Ajout d&#39;un destinataire à l&#39;envoi
*DestinatairesApi* | [**deleteAllRecipientsOfSending**](docs/Api/DestinatairesApi.md#deleteallrecipientsofsending) | **DELETE** /sendings/{sending_id}/recipients | Suppression de tous les destinataires
*DestinatairesApi* | [**deleteRecipientOfSending**](docs/Api/DestinatairesApi.md#deleterecipientofsending) | **DELETE** /sendings/{sending_id}/recipients/{recipient_id} | Suppression d&#39;un destinataire
*DestinatairesApi* | [**downloadAcknowledgementOfReceiptOfRecipientOfSending**](docs/Api/DestinatairesApi.md#downloadacknowledgementofreceiptofrecipientofsending) | **GET** /sendings/{sending_id}/recipients/{recipient_id}/download_acknowledgement_of_receipt | Télécharger l&#39;avis de réception archivé du destinataire
*DestinatairesApi* | [**downloadArchivedUndeliveredMailOfRecipientOfSending**](docs/Api/DestinatairesApi.md#downloadarchivedundeliveredmailofrecipientofsending) | **GET** /sendings/{sending_id}/recipients/{recipient_id}/download_undelivered_mail | Téléchargement de l&#39;image du pli retourné
*DestinatairesApi* | [**downloadContentProofOfRecipientOfSending**](docs/Api/DestinatairesApi.md#downloadcontentproofofrecipientofsending) | **GET** /sendings/{sending_id}/recipients/{recipient_id}/download_content_proof | Télécharger la preuve de contenu du destinataire
*DestinatairesApi* | [**downloadDepositProofOfRecipientOfSending**](docs/Api/DestinatairesApi.md#downloaddepositproofofrecipientofsending) | **GET** /sendings/{sending_id}/recipients/{recipient_id}/download_deposit_proof | Télécharger la preuve de dépôt du destinataire
*DestinatairesApi* | [**downloadEmbeddedDocumentOfRecipientOfSending**](docs/Api/DestinatairesApi.md#downloadembeddeddocumentofrecipientofsending) | **GET** /sendings/{sending_id}/recipients/{recipient_id}/content_proof/download_embedded_document | Télécharger le document contenu dans la preuve de contenu
*DestinatairesApi* | [**getAllRecipientsOfSending**](docs/Api/DestinatairesApi.md#getallrecipientsofsending) | **GET** /sendings/{sending_id}/recipients | Liste des destinataires d&#39;un envoi
*DestinatairesApi* | [**getRecipientOfSending**](docs/Api/DestinatairesApi.md#getrecipientofsending) | **GET** /sendings/{sending_id}/recipients/{recipient_id} | Détail d&#39;un destinataire
*DestinatairesApi* | [**patchRecipientOfSending**](docs/Api/DestinatairesApi.md#patchrecipientofsending) | **PATCH** /sendings/{sending_id}/recipients/{recipient_id} | Modification partielle d&#39;un destinataire
*DocumentsApi* | [**deleteDocumentOfSending**](docs/Api/DocumentsApi.md#deletedocumentofsending) | **DELETE** /sendings/{sending_id}/documents/{document_id} | Suppression d&#39;un document
*DocumentsApi* | [**getAllDocumentsOfSending**](docs/Api/DocumentsApi.md#getalldocumentsofsending) | **GET** /sendings/{sending_id}/documents | Liste des documents d&#39;un envoi
*DocumentsApi* | [**getDocumentOfSending**](docs/Api/DocumentsApi.md#getdocumentofsending) | **GET** /sendings/{sending_id}/documents/{document_id} | Détail d&#39;un document
*DocumentsApi* | [**postDocumentOfSending**](docs/Api/DocumentsApi.md#postdocumentofsending) | **POST** /sendings/{sending_id}/documents | Ajout d&#39;un document à l&#39;envoi.
*DocumentsApi* | [**setDocumentPriorityOfSending**](docs/Api/DocumentsApi.md#setdocumentpriorityofsending) | **POST** /sendings/{sending_id}/documents/{document_id}/set_priority | Classement des documents
*EnvoiApi* | [**deleteSending**](docs/Api/EnvoiApi.md#deletesending) | **DELETE** /sendings/{sending_id} | Suppression d&#39;un envoi
*EnvoiApi* | [**getAllSendings**](docs/Api/EnvoiApi.md#getallsendings) | **GET** /sendings | Liste des envois
*EnvoiApi* | [**getSending**](docs/Api/EnvoiApi.md#getsending) | **GET** /sendings/{sending_id} | Détail d&#39;un envoi
*EnvoiApi* | [**postSending**](docs/Api/EnvoiApi.md#postsending) | **POST** /sendings | Création d&#39;un envoi
*EnvoiApi* | [**submitSending**](docs/Api/EnvoiApi.md#submitsending) | **POST** /sendings/{sending_id}/submit | Finalisation d&#39;un envoi
*EnvoiApi* | [**updateSending**](docs/Api/EnvoiApi.md#updatesending) | **PATCH** /sendings/{sending_id} | Modification partielle d&#39;un envoi
*PreuveDeDptGlobaleApi* | [**downloadGlobalDepositProof**](docs/Api/PreuveDeDptGlobaleApi.md#downloadglobaldepositproof) | **GET** /global_deposit_proofs/{global_deposit_proof_id}/download | Télécharger une preuve de dépôt globale
*PreuveDeDptGlobaleApi* | [**getAllGlobalDepositProofs**](docs/Api/PreuveDeDptGlobaleApi.md#getallglobaldepositproofs) | **GET** /global_deposit_proofs | Liste des preuves de dépôt globales
*PreuveDeDptGlobaleApi* | [**getGlobalDepositProof**](docs/Api/PreuveDeDptGlobaleApi.md#getglobaldepositproof) | **GET** /global_deposit_proofs/{global_deposit_proof_id} | Détail d&#39;une preuve de dépôt globale

## Models

- [DeliveryStatus](docs/Model/DeliveryStatus.md)
- [DocumentMetadata](docs/Model/DocumentMetadata.md)
- [DocumentResponse](docs/Model/DocumentResponse.md)
- [DocumentStatus](docs/Model/DocumentStatus.md)
- [DocumentsOverrideItem](docs/Model/DocumentsOverrideItem.md)
- [DocumentsResponse](docs/Model/DocumentsResponse.md)
- [EnvelopeType](docs/Model/EnvelopeType.md)
- [ErrorResponse](docs/Model/ErrorResponse.md)
- [ErrorsResponse](docs/Model/ErrorsResponse.md)
- [GlobalDepositProofResponse](docs/Model/GlobalDepositProofResponse.md)
- [GlobalDepositProofsResponse](docs/Model/GlobalDepositProofsResponse.md)
- [MainDeliveryStatusResponse](docs/Model/MainDeliveryStatusResponse.md)
- [Mapping](docs/Model/Mapping.md)
- [MappingDataInner](docs/Model/MappingDataInner.md)
- [PagingResponse](docs/Model/PagingResponse.md)
- [PostageClass](docs/Model/PostageClass.md)
- [Priority](docs/Model/Priority.md)
- [RecipientCreation](docs/Model/RecipientCreation.md)
- [RecipientResponse](docs/Model/RecipientResponse.md)
- [RecipientStatus](docs/Model/RecipientStatus.md)
- [RecipientUpdate](docs/Model/RecipientUpdate.md)
- [RecipientsCounts](docs/Model/RecipientsCounts.md)
- [RecipientsResponse](docs/Model/RecipientsResponse.md)
- [SendingCreation](docs/Model/SendingCreation.md)
- [SendingResponse](docs/Model/SendingResponse.md)
- [SendingStatus](docs/Model/SendingStatus.md)
- [SendingUpdate](docs/Model/SendingUpdate.md)
- [SendingsResponse](docs/Model/SendingsResponse.md)
- [Statuses](docs/Model/Statuses.md)

## Authorization

Authentication schemes defined for the API:
### bearerAuth

- **Type**: Bearer authentication (JWT)

### oAuth2PasswordProduction

- **Type**: `OAuth`
- **Flow**: `password`
- **Authorization URL**: ``
- **Scopes**: 
    - **all**: CRUD on resources

### oAuth2PasswordSandbox

- **Type**: `OAuth`
- **Flow**: `password`
- **Authorization URL**: ``
- **Scopes**: 
    - **all**: CRUD on resources

## Tests

To run the tests, use:

```bash
composer install
vendor/bin/phpunit
```

## Author



## About this package

This PHP package is automatically generated by the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: `4.4`
- Build package: `org.openapitools.codegen.languages.PhpClientCodegen`
